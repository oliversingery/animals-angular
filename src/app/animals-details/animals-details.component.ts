import {Component, Input, OnInit} from '@angular/core';

@Component({
    selector: 'app-animals-details',
    templateUrl: './animals-details.component.html',
    styleUrls: ['./animals-details.component.css']
})
export class AnimalsDetailsComponent implements OnInit {

    @Input() animal;

    constructor() {
    }

    ngOnInit() {
    }

}
