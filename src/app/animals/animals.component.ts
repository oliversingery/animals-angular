import {Component, OnInit} from '@angular/core';
import {DataService} from '../data/data.service';

@Component({
    selector: 'app-animals',
    templateUrl: './animals.component.html',
    styleUrls: ['./animals.component.css']
})
export class AnimalsComponent implements OnInit {

    animals: any;
    selectedAnimal;

    constructor(private data: DataService) {
    }

    selectAnimal(animal: { name: string, weight: string, speed: string, img: string }) {
        this.selectedAnimal = animal;
    }

    ngOnInit() {
        this.data.getAnimals()
            .subscribe(data => {
                this.animals = data;
                this.selectedAnimal = data[0];
            });
    }

}
