import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {map} from 'rxjs/internal/operators';

@Injectable({
    providedIn: 'root'
})
export class DataService {

    assetDir = '/assets/json';

    constructor(private http: HttpClient) {
    }

    public getAnimals() {
        return this.http.get(`${this.assetDir}/animals.json`);

    }
}
